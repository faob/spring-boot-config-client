package hello;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class ConfigController {

  @Value("${spring.datasource.driver-class-name}")
  private String driverClassName;
 
  @Value("${spring.datasource.password}")
  private String password;
  
  @RequestMapping("/config")
  public Config greeting() throws Exception {
    Config config = new Config(driverClassName, password);
    //Info as we don't need to capture this level of details all the time
    log.info("Configured driver: {} with passord: {}", config.getDriverClassName(), config.getPassword());
    
    try {
        return config;
    } catch (Exception ex) {
      //Error level to capture the details of the exception
      log.error(ex.toString());
      throw ex;
    }
  }
}
