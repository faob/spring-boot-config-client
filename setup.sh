sudo yum install zip && \
# Install Oracle Java
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=xxx; oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u171-b11/512cd62ec5174c3487ac17c61aaa89e8/jdk-8u171-linux-x64.rpm && \
sudo rpm -i jdk-8u171-linux-x64.rpm && \
rm ./jdk-8u171-linux-x64.rpm && \
echo 'export JAVA_HOME="/usr/java/default"' >> ~/.bashrc && \
source ~/.bashrc && \
# Install Maven
sudo wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo && \
sudo sed -i s/\$releasever/6/g /etc/yum.repos.d/epel-apache-maven.repo && \
sudo yum install -y apache-maven && \
mvn --version
# Install pcf-cli
sudo yum update -y && \
sudo wget -O /etc/yum.repos.d/cloudfoundry-cli.repo https://packages.cloudfoundry.org/fedora/cloudfoundry-cli.repo && \
sudo yum install cf-cli -y && \
cf --version
