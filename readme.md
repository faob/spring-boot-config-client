# spring-boot-config-client

## Overview

* The repository is a sample client for [Spring Boot Config Server](../../../spring-boot-config-server)
* The objective is to demo how to auto-configure the client using Spring Boot Server

## The CloudIDE

1. This repository has been authored and tested using Cloud9 IDE as PoC for an isolated environment
2. All the dependencies have been installed and encapsulated in the setup.sh script

## Development Setup on CentOS (tested on Cloud9 IDE)

1. [Original post](http://www.baeldung.com/spring-cloud-configuration)
1. Clone the repository
1. `cd` into the repository folder
1. Run the setup script:
```
chmod +x ./setup.sh && \
./setup.sh
```

## How configuration is fetched

* There are configurations in `./src/main/resources/bootstrap.properties` where to find Spring Boot Server and how to authenticate:
```
spring.cloud.config.uri=http://localhost:8888
spring.cloud.config.username=root
spring.cloud.config.password=s3cr3t
spring.cloud.config.name=customerprofile
spring.cloud.config.label=master
```

## What about encrypted values?

* Spring Boot Server will auto-decrypt the encrypted values as long as the client is authenticated to the server

## To run the service
```
./mvnw spring-boot:run
```

## To package the service:
```
./mvnw clean package
```

## To run the jar:
```
java -jar ./target/spring-boot-config-client-0.1.0.jar
```

## To test the end-point

* **Please Note:** password is returned in clear-text for decryption demo purposes only!
```
curl http://localhost:8080/config
```

## To deploy the service to pcf:
```
cf login -a api.run.pivotal.io
cf push -n spring-boot-config-client -p ./target/spring-boot-config-client-0.1.0.jar   
```